#!/usr/bin/python
SCORE="cam_score"
import numpy
import math
from optparse import OptionParser
import csv
import os

#TODO:hasty job, reread

def transform(pdbfilename, rot, trans):
    '''
    Transform PDB file with rotation and translation matrix
    without changing anything in the file but coordinates.

    rot - 3x3 list of lists or numpy array e.g.:
      [[-0.037084, -0.983558, 0.176743],[-0.023889, 0.177686, 0.983797],[-0.999027, 0.032261, -0.030086]]
    trans - list of len 3 or numpy array , e.g.:
      [0.090, 67.385, -2.190]

    Return string with new transformed PDB
    '''
    rot = numpy.array(rot, dtype = float)
    trans = numpy.array(trans, dtype = float)
    outlines = []
    with open(pdbfilename) as f:
        for line in f:
            if line[0:6] in ('ATOM  ', 'HETATM'):
                x = line[30:38]
                y = line[38:46]
                z = line[46:54]
                coord = numpy.array((x, y, z), "f")
                coord = numpy.dot(coord, rot)+trans
                # coord = trans + numpy.dot(coord, rot)
                new_x = '%8.3f' % (coord[0])
                new_y = '%8.3f' % (coord[1])
                new_z = '%8.3f' % (coord[2])
                line = line[:30] + new_x + new_y + new_z + line[54:]
            outlines.append(line)
    return ''.join(outlines)

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i+n]

def read_solutions(solutions_filename, n=None):
    with open(solutions_filename, 'rU') as csvfile:
        dialect = csv.Sniffer().sniff(csvfile.readline(), [','])
        csvfile.seek(0)
        solutions = csv.DictReader(csvfile, dialect=dialect)
        for i, row in enumerate(solutions):
            if i in n:
            	row['rotation'] = list(map(float, row['rotation'].split()))
                row['translation'] = list(map(float, row['translation'].split()))
                yield row

def callback(option, opt, value, parser):
    if ".." in value:
	_from,_to=list(map(int,value.split("..")))
	ids=range(_from,_to+1)
    else:
        ids=[int(v) for v in value.split(",")]
    setattr(parser.values, option.dest, ids)

def main():
    usage = "usage: %prog [options] solutions.txt ori_pdb"
    parser = OptionParser(usage=usage)
    parser.add_option("-l", type="string", dest="solution_ids", action='callback',
                      help="List of solution ids of solutions to be generated",callback=callback)
    parser.add_option("-o", "--outdir", dest="outdir", default=os.getcwd(),
                      help="optional DIR with output", metavar="DIR")
    
    (options, args) = parser.parse_args()
    solutions_fn = args[0]
    pdb_fn = args[1]
    for sol in read_solutions(solutions_fn,n=options.solution_ids):
        rot = list(chunks(sol['rotation'], 3))
        newrot = [
            [],
            [],
            []
        ]
        for row in rot:
            for i in range(3):
                newrot[i].append(row[i])
        rot = newrot
        pdblines = transform(pdb_fn, rot, sol['translation'])
        outpdb_fn = os.path.join(options.outdir, sol['filename'])
        with open(outpdb_fn, 'w') as outpdb_f:
            outpdb_f.write(''.join(pdblines))
	print("PDB file %s created in %s ..."%(outpdb_fn,options.outdir)) 

if __name__ == '__main__':
    main()
